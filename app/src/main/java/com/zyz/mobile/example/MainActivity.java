package com.zyz.mobile.example;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;

public class MainActivity extends Activity {

//	private SelectableTextView mTextView;
	private int mTouchX;
	private int mTouchY;
	private final static int DEFAULT_SELECTION_LEN = 5;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

//        LinearLayout linear = (LinearLayout) findViewById(R.id.linear);
//        linear.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                return true;
//            }
//        });

        // make sure the TextView's BufferType is Spannable, see the main.xml




//        LinearLayout linear = (LinearLayout) findViewById(R.id.linear);
//
//        SelectableTextView stv = (SelectableTextView) findViewById(R.id.main_text);
//        prepareTextView(stv);
//
//        stv = (SelectableTextView) View.inflate(this, R.layout.list_item, null);
//        prepareTextView(stv);
//        linear.addView(stv);
//        linear.invalidate();


        /*ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(new BaseAdapter() {

            @Override
            public int getCount() {
                return 10;
            }

            @Override
            public Object getItem(int i) {
                return null;
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                if(view == null)
                {
                    view = View.inflate(MainActivity.this, R.layout.list_item, null
                    );
                }
                final SelectableTextView mTextView = (SelectableTextView) view;


                return view;
            }
        });*/

	}

	private void showSelectionCursors(int x, int y, SelectableTextView textView) {
		int start = textView.getPreciseOffset(x, y);

		if (start > -1) {
			int end = start + DEFAULT_SELECTION_LEN;
			if (end >= textView.getText().length()) {
				end = textView.getText().length() - 1;
			}
			textView.showSelectionControls(start, end);
		}
	}

    private void prepareTextView(final SelectableTextView mTextView){
        mTextView.setDefaultSelectionColor(0x40FF00FF);


        mTextView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showSelectionCursors(mTouchX, mTouchY, mTextView);
                return true;
            }
        });
        mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextView.hideCursor();
            }
        });
        mTextView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mTouchX = (int) event.getX();
                mTouchY = (int) event.getY();
                return false;
            }
        });

    }
}

