package selectable;

import android.content.Context;
import android.gesture.Gesture;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.Toast;

import com.zyz.mobile.example.R;
import com.zyz.mobile.example.SelectableTextView;

/**
 * Created by mnorouzi on 10/22/2014.
 */
public class SelectableScrollView extends ScrollView {
    private final int[] mTempCoords = new int[2];
    private SelectionCursorController mController;
    GestureDetector longPressGesture;

    public SelectableScrollView(Context context) {
        super(context);
        init();
    }

    public SelectableScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SelectableScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        longPressGesture = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return super.onSingleTapUp(e);
            }

            @Override
            public void onLongPress(MotionEvent e) {
                if (mController == null) {
                    mController = new SelectionCursorController();
                }
                mController.show(new Point(10,10), new Point(20,20));
                super.onLongPress(e);
            }

//            @Override
//            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
//                Log.i("onScroll", String.format("x=%f , y=%f", distanceX, distanceY));
//                return super.onScroll(e1, e2, distanceX, distanceY);
//            }
        });
        longPressGesture.setIsLongpressEnabled(true);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return longPressGesture.onTouchEvent(ev) || super.onTouchEvent(ev);
    }


    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        if(mController != null)
            mController.snapToSelection();
        super.onScrollChanged(l, t, oldl, oldt);
    }

    private class SelectionCursorController implements ViewTreeObserver.OnTouchModeChangeListener {

        /**
         * The two cursor handle that controls the selection. Note that the two cursors are allowed to
         * swap positions and thus the name of the handle has no bearing on the relative position of the
         * handle to each other. (e.g. mStartHandle can be positioned leagally at an offset greater than
         * the offset mEndHandle is resting on)
         */
        private CursorHandle mStartHandle;
        private CursorHandle mEndHandle;
        private int mOldScrollY = 0;


        /**
         * whether the selection controller is displaying on the screen
         */
        private boolean mIsShowing;


        public SelectionCursorController() {
            mStartHandle = new CursorHandle(this);
            mEndHandle = new CursorHandle(this);
        }


        /**
         * snap the cursors to the current selection
         */
        public void snapToSelection() {

            if (mIsShowing) {
//                int a = SelectableTextView.this.getSelection().getStart();
//                int b = SelectableTextView.this.getSelection().getEnd();

//                int start = Math.min(a, b);
//                int end = Math.max(a, b);

                // find the corresponding handle for the start/end calculated above
//                CursorHandle startHandle = start == a ? mStartHandle : mEndHandle;
//                CursorHandle endHandle = end == b ? mEndHandle : mStartHandle;

                final int[] coords = mTempCoords;
                int scrollY = SelectableScrollView.this.getScrollY();
                if(mOldScrollY == 0) {
                    mOldScrollY = scrollY;
                }
//                int scrollYOffset = scrollY;

                Log.i("scroll", String.format("scroll y=%d",scrollY));
//
                mStartHandle.moveToY(mOldScrollY-scrollY);
                mEndHandle.moveToY(mOldScrollY-scrollY);

                mOldScrollY = scrollY;
//
//
////                SelectableScrollView.this.getAdjusteStartXY(start, scroll_y, coords);
//                mStartHandle.pointTo(coords[0], coords[1]);
//
////                SelectableScrollView.this.getAdjustedEndXY(end, scroll_y, coords);
//                mEndHandle.pointTo(coords[0], coords[1]);
            }
        }

        public void show(Point start, Point end){
            mStartHandle.show(start.x, start.y);
            mEndHandle.show(end.x, end.y);

            mIsShowing = true;
        }

//        /**
//         * show the selection cursor at the specified offsets and select the text between the specified
//         * offsets.
//         *
//         * @param start the offset of the first cursor
//         * @param end   the offset of the second cursor
//         */
//        public void show(int start, int end) {
//
//            int a = Math.min(start, end);
//            int b = Math.max(start, end);
//
//            final int[] coords = mTempCoords;
//            int scroll_y = SelectableScrollView.this.getScrollY();
//
////            SelectableTextView.this.getAdjusteStartXY(a, scroll_y, coords);
//            mStartHandle.show(coords[0], coords[1]);
//
////            SelectableTextView.this.getAdjustedEndXY(b, scroll_y, coords);
//            mEndHandle.show(coords[0], coords[1]);
//
//            mIsShowing = true;
////            select(a, b);
//        }


        /**
         * hide the cursor and selection
         */
        public void hide() {
            if (mIsShowing) {
//                SelectableTextView.this.removeSelection();
                mStartHandle.hide();
                mEndHandle.hide();
                mIsShowing = false;
            }
        }


        /**
         * redraw the moving cursor and update the selection if required
         *
         * @param cursorHandle the CursorHandle (LEFT or RIGHT)
         * @param x            the x coordinate the cursor is pointing to on the screen (raw)
         * @param y            the y coordinate the cursor is pointing to on the screen (raw)
         * @param oldx         the previous x position the cursor pointed to
         * @param oldy         the previous y position the cursor pointed to
         */
        public void updatePosition(CursorHandle cursorHandle, int x, int y, int oldx, int oldy) {
            if (!mIsShowing) {
                return;
            }

//            int old_offset =
//                    cursorHandle == mStartHandle ?
//                            SelectableTextView.this.getSelection().getStart() :
//                            SelectableTextView.this.getSelection().getEnd();
//
//            int offset = SelectableTextView.this.getHysteresisOffset(x, y, old_offset);
//
//            if (offset != old_offset) {
//
//                if (cursorHandle == mStartHandle) {
//                    SelectableTextView.this.getSelection().setStart(offset);
//                } else {
//                    SelectableTextView.this.getSelection().setEnd(offset);
//                }
//                SelectableTextView.this.getSelection().select();
//            }

            cursorHandle.pointTo(x, y);
        }

//        /**
//         * Select the textview between start and end.
//         * <p/>
//         * Precondition: start, end must be valid
//         *
//         * @param start the starting offset
//         * @param end   the ending offset
//         */
//        private void select(int start, int end) {
////            SelectableTextView.this.setSelection(Math.min(start, end), Math.abs(end - start));
//        }

        @SuppressWarnings("unused")
        public boolean isShowing() {
            return mIsShowing;
        }


        @Override
        public void onTouchModeChanged(boolean isInTouchMode) {
            if (!isInTouchMode) {
                hide();
            }
        }
    }


    /**
     * represents a single cursor
     */
    private class CursorHandle extends View {

        /**
         * the {@link android.widget.PopupWindow} containing the cursor drawable
         */
        private final PopupWindow mContainer;

        /**
         * the drawble of the cursor
         */
        private Drawable mDrawable;

        /**
         * whether the user is dragging the cursor
         */
        @SuppressWarnings("unused")
        private boolean mIsDragging;

        /**
         * the controller that's controlling the cursor
         */
        private SelectionCursorController mController;

        /**
         * the height of the cursor
         */
        private int mHeight;

        /**
         * the width of the cursor
         */
        private int mWidth;

        /**
         * the x coordinate of the "pointer" of the cursor
         */
        private int mHotspotX;

        /**
         * the y coordinate of the "pointer" of the cursor which is usually the top, so it's zero.
         */
        private int mHotspotY;


        /**
         * Adjustment to add to the Raw x, y coordinate of the touch position to get the location of where
         * the cursor is pointing to
         */
        private int mAdjustX;
        private int mAdjustY;

        private int mOldX;
        private int mOldY;

        public CursorHandle(SelectionCursorController controller) {
            super(SelectableScrollView.this.getContext());

            mController = controller;

            mDrawable = getResources().getDrawable(R.drawable.cursor);

            mContainer = new PopupWindow(this);
            mContainer.setClippingEnabled(false);

			/* My Note
            getIntrinsicWidth() returns the width of the drawable after it has been
			scaled to the current device's density
			e.g. if the drawable is a 15 x 20 image and we load the image on a Nexus 4 (which
			has a density of 2.0), getIntrinsicWidth() shall return 15 * 2 = 30
			 */
            mHeight = mDrawable.getIntrinsicHeight();
            mWidth = mDrawable.getIntrinsicWidth();

            // the PopupWindow has an initial dimension of (0, 0)
            // must set the width/height of the popupwindow in order for it to be drawn
            mContainer.setWidth(mWidth);
            mContainer.setHeight(mHeight);

            // this is the location of where the pointer is relative to the cursor itself
            // if the left and right cursor are different, mHotspotX will need to be calculated
            // differently for each cursor. Currently, I'm using the same left and right cursor
            mHotspotX = mWidth / 2;
            mHotspotY = 0;

            invalidate();
        }


        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            setMeasuredDimension(mWidth, mHeight);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            mDrawable.setBounds(0, 0, mWidth, mHeight);
            mDrawable.draw(canvas);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {

            int rawX = (int) event.getRawX();
            int rawY = (int) event.getRawY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    // calculate distance from the (x,y) of the finger to where the cursor
                    // points to
                    mAdjustX = mHotspotX - (int) event.getX();
                    mAdjustY = mHotspotY - (int) event.getY();
                    mOldX = mAdjustX + rawX;
                    mOldY = mAdjustY + rawY;

                    mIsDragging = true;
                    break;
                }
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL: {
                    mIsDragging = false;
                    mController.snapToSelection();
                    break;
                }
                case MotionEvent.ACTION_MOVE: {
                    // calculate the raw (x, y) the cursor is POINTING TO
                    int x = mAdjustX + rawX;
                    int y = mAdjustY + rawY;

                    mController.updatePosition(this, x, y, mOldX, mOldY);

                    mOldX = x;
                    mOldY = y;
                    break;
                }
            }
            return true; // consume the event
        }


        public boolean isShowing() {
            return mContainer.isShowing();
        }

        /**
         * Show the cursor pointing to the specified point.
         *
         * @param x the x coordinate of the point relative to the TextView
         * @param y the y coordinate of the point relative to the TextView
         */
        public void show(int x, int y) {
            final int[] coords = mTempCoords;
            SelectableScrollView.this.getLocationInWindow(coords);

            coords[0] += x - mHotspotX;
            coords[1] += y - mHotspotY;
            mContainer.showAtLocation(SelectableScrollView.this, Gravity.NO_GRAVITY, coords[0], coords[1]);
        }

        /**
         * move the cursor to point the (x,y) location on the screen.
         *
         * @param x the x coordinate on the screen
         * @param y the y coordinate on the screen
         */
        private void pointTo(int x, int y) {
            if (isShowing()) {
                mContainer.update(x - mHotspotX, y - mHotspotY, -1, -1);
            }
        }


        private void moveToY(int offsetY){
            if (isShowing()) {
                int y = mOldY + offsetY;
                mController.updatePosition(this, mOldX, y, mOldX, mOldY);
                mOldY = y;
//                mContainer.update(mOldX - mHotspotX, mOldY - mHotspotY, -1, -1);
            }
        }

        /**
         * hide this cursor
         */
        public void hide() {
            mIsDragging = false;
            mContainer.dismiss();
        }

    }
}
